(ns honeysql-plus.spec.sql
  (:require [clojure.spec.alpha :as s]
            [honeysql-plus.core :as hh+]))

(s/fdef hh+/sql-quote-string
        :args (s/cat :field string?)
        :ret string?)